# Gunakan image NGINX resmi sebagai image dasar
FROM nginx:alpine

# Hapus konten default NGINX
RUN rm -rf /usr/share/nginx/html/*

# Salin konten web statis ke direktori NGINX
COPY . /usr/share/nginx/html/

# Expose port 80
EXPOSE 80

# Perintah untuk menjalankan NGINX
CMD ["nginx", "-g", "daemon off;"]
